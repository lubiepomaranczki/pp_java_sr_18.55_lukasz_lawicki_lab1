package lab1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Euler42 {

	public static String[] pobierzDane() throws FileNotFoundException
	{
		File plik = new File("slowa.txt");
		Scanner odczyt = new Scanner(plik);
		//wczytujemy kazdy wiersz do zmiennej wiersz
		String wiersz = odczyt.nextLine();
		//nastepnie dzielimy wiersz na slowa
		String[] slowa = wiersz.split(",");
		odczyt.close();
		
		return slowa;
	}
	
	
	public static boolean czySlowoTrojkatne(String slowo)
	{
	int suma = 0;
	String alfabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	for(int i = 0 ; i < slowo.length() ; i++)
	{			
		for(int j = 0 ; j < alfabet.length() ; j++)
		{
			if(slowo.charAt(i) == alfabet.charAt(j))
					suma = (j+1) +suma;
		}
	}
		int tr=0;
		for(int i=1 ; tr<suma ; i++)
			tr = (i*(i + 1))/2;
			
		
		if(tr == suma)
			return true;
		
		return false;

	}
	
	public static int ileSlow() throws FileNotFoundException
	{
		int ileSlow=0;
		String[] temp = pobierzDane();
		
		for(int i = 0; i < temp.length ; i++)
		{
			if(czySlowoTrojkatne(temp[i])==true)
			{
				ileSlow++;
				//jak chcesz wy�wietli� s�owa czy dzia�aj� to odkomentuj to ni�ej
				//System.out.println(temp[i]);
			}
		
		}
		
		return ileSlow;
	}
	/*
	public static void main(String[] args) throws FileNotFoundException 
	{
		// TODO Auto-generated method stub
		int ileSlow=0;
		ileSlow = ileSlow();
		
		System.out.println(ileSlow);

	}
*/
}

