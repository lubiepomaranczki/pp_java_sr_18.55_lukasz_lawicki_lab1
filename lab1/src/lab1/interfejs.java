package lab1;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.FileNotFoundException;
import java.awt.Color;
import java.awt.Font;

public class interfejs {

	private JFrame frmukaszawickiLab;
	private JTextField rozw_text;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					interfejs window = new interfejs();
					window.frmukaszawickiLab.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public interfejs() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmukaszawickiLab = new JFrame();
		frmukaszawickiLab.setTitle("\u0141ukasz \u0141awicki Lab1");
		frmukaszawickiLab.getContentPane().setBackground(Color.GRAY);
		frmukaszawickiLab.setBounds(100, 100, 450, 300);
		frmukaszawickiLab.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmukaszawickiLab.getContentPane().setLayout(null);
		
		JButton euler42_but = new JButton("Ilo\u015B\u0107 s\u0142\u00F3w tr\u00F3jk\u0105tnych");
		euler42_but.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					rozw_text.setText(String.valueOf(Euler42.ileSlow()));
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		euler42_but.setBounds(130, 124, 190, 23);
		frmukaszawickiLab.getContentPane().add(euler42_but);
		
		JButton fibo_but = new JButton("Suma 4kk wyraz\u00F3w ci\u0105gu");
		fibo_but.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				rozw_text.setText(String.valueOf(Fibonacci.suma(4000000)));
			}
		});
		fibo_but.setBounds(130, 74, 190, 23);
		frmukaszawickiLab.getContentPane().add(fibo_but);
		
		JLabel lblProsteGuiUytkownika = new JLabel("Proste GUI u\u017Cytkownika");
		lblProsteGuiUytkownika.setForeground(Color.WHITE);
		lblProsteGuiUytkownika.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblProsteGuiUytkownika.setBounds(10, 13, 255, 50);
		frmukaszawickiLab.getContentPane().add(lblProsteGuiUytkownika);
		
		rozw_text = new JTextField();
		rozw_text.setBounds(294, 229, 130, 23);
		frmukaszawickiLab.getContentPane().add(rozw_text);
		rozw_text.setColumns(10);
		
		JLabel lblRozwizanie = new JLabel("Rozwi\u0105zanie:");
		lblRozwizanie.setForeground(Color.WHITE);
		lblRozwizanie.setBackground(Color.WHITE);
		lblRozwizanie.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblRozwizanie.setBounds(207, 229, 78, 23);
		frmukaszawickiLab.getContentPane().add(lblRozwizanie);
	}
}
